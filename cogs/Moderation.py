"""moderation commands!!!"""
import logging
import discord
from discord.ext import commands

# pylint: disable=W1202

#logger stuff yeah
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER_HANDLER = logging.FileHandler(filename="logs/{}.log".format(__name__), encoding='utf-8', mode='w')
LOGGER_HANDLER.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
LOGGER.addHandler(LOGGER_HANDLER)

class Moderation:
    """moderation commands, require permissions"""

    def __init__(self, bot):
        self.bot = bot

    async def __after_invoke(self, ctx):
        LOGGER.info('{0.command} is done...'.format(ctx))

    @commands.command()
    async def purge(self, ctx, member: discord.Member, mess_number: int):
        """Removes messages sent by a user. (max 100)"""
        def is_member(mess):
            """bad docstring"""
            return mess.author == member
        try:
            deleted = await ctx.message.channel.purge(limit=mess_number, check=is_member)
            await ctx.send('Deleted {} message(s) from this channel.'.format(len(deleted)))
        except discord.Forbidden:
            pass

    @commands.command()
    async def addrole(self, ctx, member: discord.Member, role: str):
        """Add a role to a user."""
        role = discord.utils.find(lambda m: m.name == role, ctx.guild.roles)
        if role is None:
            await ctx.send('The role "**{}**" does not exist.'.format(role))
        else:
            try:
                await member.add_roles(role, 
                                       reason="Command invoked by {}. (self-bot)".format(ctx.message.author.name))
            except discord.Forbidden:
                pass


    @commands.command()
    async def removerole(self, ctx, member: discord.Member, role: str):
        """Remove a role from a user."""
        role = discord.utils.find(lambda m: m.name == role, ctx.guild.roles)
        if role is None:
            await ctx.send('The role "**{}**" does not exist.'.format(role))
        else:
            try:
                await member.remove_roles(role, 
                                          reason="Command invoked by {} (self-bot).".format(ctx.message.author.name))
            except discord.Forbidden:
                pass


def setup(bot):
    """adds the cog as a cog"""
    bot.add_cog(Moderation(bot))
    