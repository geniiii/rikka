"""ok"""
import json
import logging
import os


from urllib import request
from urllib.request import urlopen

import discord
from discord.ext import commands
import requests

# pylint: disable=W1202
# pylint: disable=W0703

#logger stuff yeah
LOGGER_PATH = os.path.dirname('__file__')
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
LOGGER_HANDLER = logging.FileHandler(filename="logs/{}.log".format(__name__), encoding='utf-8', mode='w')
LOGGER_HANDLER.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
LOGGER.addHandler(LOGGER_HANDLER)

class Info:
    """info cog class"""

    def __init__(self, bot):
        self.bot = bot
        self.thingy = os.path.dirname('__file__')
        self.config = os.path.join(self.thingy, "config/config.json")
        self.rikka = os.path.join(self.thingy, "config/rikka.json")
        self.osu_config = os.path.join(self.thingy, "config/osu.json")

    async def __after_invoke(self, ctx):
        LOGGER.info('{0.command} is done...'.format(ctx))



    @commands.command()
    async def libs(self, ctx):
        """Sends the libraries used in rikka"""
        color = discord.Colour.green()
        with open(self.rikka) as file:
            data = json.load(file)
            desc = data['libs']
        titi = discord.Embed(title="Libraries used in Rikka.",
                             description="Rikka was made with the help of:\n" + desc,
                             colour=color)
        await ctx.send(embed=titi)

    @commands.command()
    async def version(self, ctx):
        """Sends the Rikka version."""
        with open(self.config) as file:
            data = json.load(file)
            ver = data['version']
        await ctx.send("Rikka (self-bot) - version {}.".format(ver))

    @commands.command()
    async def userinfo(self, ctx, *, user: discord.Member):
        """Shows info about a user."""
        created_at = user.created_at.replace(microsecond=0)
        joined_at = user.joined_at.replace(microsecond=0)
        info = {"Name:": user.name,
                "Avatar:": user.avatar_url,
                "Bot:": str(user.bot),
                "Created at:": created_at,
                "Joined at:": joined_at,
                "Game:": str(user.game),
                "Nickname": str(user.nick),
                "Highest role:": str(user.top_role)}
        embed = discord.Embed(title=info["Name:"], colour=user.colour)
        embed.set_image(url=info["Avatar:"])
        for key in info:
            if key in ["Name:", "Avatar:"]:
                continue
            val = info[key]
            embed.add_field(name=key, value=val)
        await ctx.send(embed=embed)

    @commands.command()
    async def serverinfo(self, ctx):
        """Shows info about the server."""
        guild = ctx.message.guild
        created_at = guild.created_at.replace(microsecond=0)
        info = {
            "Created at:": created_at,
            "Name:": str(guild.name),
            "Owner:": str(ctx.message.guild.owner),
            "Verification Level:": str(guild.verification_level),
            "Icon:": str(guild.icon_url),
            "Role count:": len(guild.roles),
            "Custom emoji count:": len(guild.emojis),
            "Channel count:": len(guild.channels),
            "Region:": str(guild.region),
            "Member count:": guild.member_count,
        }
        embed = discord.Embed(title=info["Name:"], colour=ctx.message.guild.owner.colour)
        embed.set_image(url=info["Icon:"])
        for key in info:
            if key in ["Name:", "Icon:"]:
                continue
            val = info[key]
            embed.add_field(name=key, value=val)
        await ctx.send(embed=embed)

    @commands.command()
    async def owinfo(self, ctx, user_id: str, region: str):
        """Shows info about an Overwatch player."""
        user_id = user_id.replace("#", "-")
        region = region.lower()
        url = "http://owapi.net/api/v3/u/" + user_id + "/blob"
        LOGGER.info("ow api url: %s", url)
        req = request.Request(url, headers={'User-Agent': "xxl sakujes browser"})
        with urlopen(req) as url:
            data = json.loads(url.read().decode())
        info = {
            "Name:": user_id.replace("-", "#"),
            "Avatar:": data[region]["stats"]["quickplay"]["overall_stats"][str("avatar")],
            "**Region: **": region.upper(),
            "**Level: **": data[region]["stats"]["quickplay"]["overall_stats"][str("level")],
            "**Rank: **": data[region]["stats"]["quickplay"]["overall_stats"]["tier"],
            "**Quick Play Wins: **": data[region]["stats"]["quickplay"]
                                     ["overall_stats"][str("wins")],
            "**Quick Play Losses: **": data[region]["stats"]["quickplay"]
                                       ["overall_stats"][str("losses")],
            "**Quick Play Eliminations: **": data[region]["stats"]["quickplay"]
                                             ["game_stats"][str("eliminations")],
            "**Quick Play Deaths: **": data[region]["stats"]["quickplay"]
                                       ["game_stats"][str("deaths")],
            "**Quick Play KDR: **": str(data[region]["stats"]["quickplay"]["game_stats"]
                                        ["eliminations"] / data[region]["stats"]
                                        ["quickplay"]["game_stats"]["deaths"]),
            }
        if data[region]["stats"]["competitive"] is not None:
            user_iddee = {
                "**Competitive Eliminations: **": data[region]["stats"]
                                                  ["competitive"]["game_stats"]
                                                  [str("eliminations")],
                "**Competitive Deaths: **": data[region]["stats"]
                                            ["competitive"]["game_stats"][str("deaths")],
                "**Competitive Wins: ** ": data[region]["stats"]
                                           ["competitive"]["overall_stats"][str("wins")],
                "**Competitive Losses: ** ": data[region]["stats"]
                                             ["competitive"]["overall_stats"][str("losses")],
                "**Competitive KDR: **": str(data[region]["stats"]
                                             ["competitive"]["game_stats"]["eliminations"] /
                                             data[region]["stats"]["competitive"]
                                             ["game_stats"]["deaths"]),
                }
            xxl = {**info, **user_iddee}
        else:
            xxl = info
        try:
            xxl["**Rank: **"] = xxl["**Rank: **"].title()
        except Exception:
            xxl["**Rank: **"] = "None"

        embed = discord.Embed(title=xxl["Name:"], colour=ctx.message.author.colour)
        embed.set_image(url=xxl["Avatar:"])
        link = ""

        if xxl["**Rank: **"] == "Bronze":
            link = "http://i.imgur.com/tHld9yz.png"
        elif xxl["**Rank: **"] == "Silver":
            link = "http://i.imgur.com/IOt3CTv.png"
        elif xxl["**Rank: **"] == "Gold":
            link = "http://i.imgur.com/Q9wGr6d.png"
        elif xxl["**Rank: **"] == "Platinum":
            link = "http://i.imgur.com/RrPCNM1.png"
        elif xxl["**Rank: **"] == "Diamond":
            link = "http://i.imgur.com/fkw2cxN.png"
        elif xxl["**Rank: **"] == "Master":
            link = "http://i.imgur.com/HPbH7uV.png"
        elif xxl["**Rank: **"] == "Grandmaster":
            link = "http://i.imgur.com/xdPsP4w.png"

        embed.set_thumbnail(url=link)

        for key in xxl.keys():
            if key in ["Name:", "Avatar:"]:
                continue
            val = xxl[key]
            embed.add_field(name=key, value=val)

        await ctx.send(embed=embed)

    @commands.command()
    async def invite(self, ctx):
        """Sends an invite URL for the bot."""
        thingy = os.path.dirname('__file__')
        config = os.path.join(thingy, "config/config.json")
        with open(config) as file: #opens the config file and saves data from it to variables
            data = json.load(file)
            invite = data['invite']
        await ctx.send("Invite URL: {}".format(invite))

    @commands.command()
    async def osu(self, ctx, *, user: str):
        """Shows info about an osu! player."""
        url = "https://osu.ppy.sh/api/get_user?k={}&u={}"
        countries = os.path.join(self.thingy, "osu/countries.json")
        with open(self.osu_config) as file:
            data = json.load(file)
            key = data['key']

        url = url.format(key, user)
        r = requests.get(url).json()


        info = {
            "user_id": r[0]["user_id"],
            "name": r[0]["username"],
            "country": r[0]["country"],
            "**Total attempts: **": r[0]["playcount"],
            "**Level: **": r[0]["level"],
            "**PP: **": r[0]["pp_raw"],
            "**Accuracy: **": r[0]["accuracy"] + '%'
        }

        with open(countries) as file:
            clist = json.load(file)
            country = clist[info["country"]]

        color = discord.Colour.from_rgb(255, 109, 238)

        em = discord.Embed(title=info["name"],
                           colour=color)

        for key in info.keys():
            if key in ["user_id", "name", "country"]:
                continue
            val = info[key]
            em.add_field(name=key, value=val)

        em.add_field(name="**Country: **", value=country)
        em.set_image(url="https://a.ppy.sh/{}".format(info["user_id"]))
        await ctx.send(embed=em)

    @commands.command()
    async def poll(self, ctx, *, question: str):
        """Makes a poll about something."""
        poll_em = discord.Embed(title="Poll",
                                description=question,
                                color=discord.Color.green())
        msg = await ctx.send(embed=poll_em)
        await msg.add_reaction('👍')
        await msg.add_reaction('👎')

def setup(bot):
    """ok"""
    bot.add_cog(Info(bot))
