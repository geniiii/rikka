"""uhh bot thing"""
import json

import os
import logging

import discord
from discord.ext import commands
from discord.ext.commands import CommandOnCooldown

# pylint: disable=W0703
# pylint: disable=W1202

def rikka(): #the bot's function
    """the main thing"""
    # config stuff
    thingy = os.path.dirname('__file__') #location of the bot
    cogs_json = os.path.join(thingy, "config/cogs.json") #cog list
    config = os.path.join(thingy, "config/config.json") #config file

    with open(config) as file: #opens the config file and saves data from it to variables
        data = json.load(file)
        prefix = data['prefix']
        description = data['description']
        token = data['token']
        version = data['version']
        log_name = data['log']

	# logging shit
    rikka_log = logging.getLogger("discord") #makes the logger
    rikka_log.setLevel(logging.INFO) #sets the logger's level
    handler = logging.FileHandler(filename="logs/{}".format(log_name), encoding='utf-8', mode='w') #puts the logger in a file (using a handler)
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')) #the log message's format
    rikka_log.addHandler(handler) #adds the handler to the logger

	# the bot (THE BATH XD) (almost all of the variables here can be set in the config)
    bot = commands.Bot( # adds the bot
        command_prefix=prefix, # the bot's prefix
        description=description, # the bot's description in the help command
        self_bot=True) # listen only to itself

    @bot.event
    async def on_ready():
        """does stuff when the bot runs yes""" #pretty much
        print("Username: {}".format(bot.user.name))
        print("ID: {}".format(str(bot.user.id)))
        print("Prefix: {}".format(bot.command_prefix))
        print("Version: {}".format(version))
        print("Description: {}".format(description))

    with open(cogs_json) as cogs_list:
        cogs = json.load(cogs_list)
        for key, value in cogs.items():
            try:
                bot.load_extension(value) #tries to load extensions
                msg = "Loaded extension {}.".format(value)
                print(msg) #prints that it loaded extension X
                rikka_log.info(msg) #logs it to the rikka.log file
            except ImportError as thing: #if it fails
                exc = "{}: {}".format(type(thing).__name__, thing) #the exception
                msg = "Failed to load extension {}\n{}".format(value, exc)
                print(msg) #prints the exception message
                rikka_log.error(msg) #logs it to the rikka.log file
            except discord.ClientException as thing:
                msg = "{} doesn't have a setup function.".format(value)
                print(msg) #prints the exception message
                rikka_log.error(msg) #logs it to the rikka.log file
            except Exception as thing:
                exc = "{}: {}".format(type(thing).__name__, thing) #the exception
                msg = "Failed to load extension {}\n{}".format(value, exc)
                print(msg) #prints the exception message
                rikka_log.error(msg) #logs it to the rikka.log file

    # debug commands
    @bot.command()
    async def load_cog(ctx, *, cog: str): # loads a cog
        """Loads a cog."""
        try:
            bot.load_extension(cog)
        except Exception as thing:
            exc = "{}: {}".format(type(thing).__name__, thing)
            msg = "```Failed to load extension {}\n{}```".format(cog, exc)
            await ctx.send(msg)

    @bot.command() # unloads a cog
    async def unload_cog(ctx, *, cog: str):
        """Unloads a cog."""
        try:
            bot.unload_extension(cog)
        except Exception as thing:
            exc = "{}: {}".format(type(thing).__name__, thing)
            msg = "```Failed to unload extension {}\n{}```".format(cog, exc)
            await ctx.send(msg)

    @bot.command() # reloads a cog
    async def reload_cog(ctx, *, cog: str):
        """Reloads a cog."""
        try:
            bot.unload_extension(cog)
            bot.load_extension(cog)
        except Exception as thing:
            exc = "{}: {}".format(type(thing).__name__, thing)
            msg = "```Failed to reload extension {}\n{}```".format(cog, exc)
            await ctx.send(msg)

    bot.run(token, bot=False)


if __name__ == "__main__":
    rikka() #runs the bot
